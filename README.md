# pairs

Image pairs - search matching images online with others

## Hinweis
Dieses Modul wird aktuell nicht mehr weiterentwickelt. 

Eine Alternative (die vom Format kompatibel ist), stellt die Anwendung "pairs" in den MintApps dar (siehe https://codeberg.org/tk100/MintApps/src/branch/main/src/mint/views/Pairs.vue). Sie erlaubt ebenso für den Spezialfall technischer und naturwissenschaftliche Fächer mathematische Formeln auf den Karten (was z.B. in h5p nicht direkt möglich ist). Man kann das Spiel zudem auch online gemeinsam gegeneinander gespielen.