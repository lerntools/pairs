var express=require('express');
var router=express.Router();
var controller=require('./controller');
var mainController=require('../../../main/server/controller');

//player
router.get('/sets-meta', controller.getSetsMeta);
router.get('/sets/:ticket', controller.loadSetFromTicket, controller.getSet);
router.get('/socket/:ticket', controller.getSocketUrl);

//teacher routes
router.get('/sets-author/:id', controller.checkAuth, controller.loadSetFromId, controller.getSet);
router.post('/sets-author',controller.checkAuth, controller.importJson);
router.put('/sets-author/:id',controller.checkAuth, controller.loadSetFromId, controller.ensureUserIsOwner, controller.importJson);
router.delete('/sets-author/:id',controller.checkAuth, controller.loadSetFromId, controller.ensureUserIsOwner,  controller.deleteSet);

//Websocket
router.ws('/ws/:instance', controller.initWs);

module.exports=router;
